def make_dict(codes):
    return dict((x, y) for x, y in codes)


def m2c(msg, codes):
    result = ""
    code_dict = make_dict(codes)
    for i in msg:
        result += code_dict[i]
    return result


def m2c_nice(msg, codes):
    result = ""
    code_dict = make_dict(codes)
    for i in msg:
        result += code_dict[i] + " "
    return result


def make_reverse_dict(codes):
    return dict((x, y) for y, x in codes)


def c2m(msg, codes):
    result = ""
    reverse_dict = make_reverse_dict(codes)
    while msg != "":
        pos = -1
        i = ""
        for i in codes:
            pos = msg.find(i[1])
            if pos == 0:
                break
        if pos != 0:
            break
        result += reverse_dict[i[1]]
        msg = msg[len(i[1]):]
    return result
