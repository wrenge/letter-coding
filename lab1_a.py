from math import log
import random


def even(m, n):  # m=63    X-Sh
    return abs(n * log2(m))


def uneven(m, n, p):
    result = 0
    for i in range(0, m):
        result += p[i] * log2(p[i])
    return abs(n * result)


def generateP(m):
    p = []
    bank = 1
    for i in range(0, m - 1):
        pi = random.uniform(0, bank)
        bank -= pi
        p.append(pi)
    p.append(bank)
    return p


def makeAlpha(message):
    dic = dict()
    for i in range(0, len(message)):
        dic[message[i]] = 0
    return dic.keys()


def log2(x):
    return log(x, 2)


# энтропия для равномерного распределения
def entropyEven(m):
    return log2(m)


# энтропия для неравномерного распределения
def entropyUneven(m, p):
    result = 0
    for i in range(0, m):
        result += p[i] * log2(p[i])
    return abs(result)


# избыточность
def REven(m):
    print(f'R = {0}')


def RUneven(m, p):
    print(f'R = {(entropyEven(m) - entropyUneven(m, p)) / entropyEven(m)}')


def char_range(c1, c2):
    for c in range(ord(c1), ord(c2) + 1):
        yield chr(c)