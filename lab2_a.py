import math


def even_code(inp):
    count_bin = math.floor(math.log2(len(inp))) + 1
    result = []
    for i in range(0, len(inp)):
        print(inp[i][0], end='\t')
        print(format(i, "b").zfill(count_bin))
        result.append((inp[i][0], format(i, "b").zfill(count_bin)))

    return result

