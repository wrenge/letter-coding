import random
import string
import lab1_a

message = " "
# alpha = makeAlpha(message)
# alpha = [1, 2]
alpha = [' ']
alpha.extend(string.ascii_lowercase)
alpha.extend(string.ascii_uppercase)
alpha.extend(range(0, 10))
# 26+26+10+1 = 63 ---> m

n = len(message)  # длина сообщения
m = len(alpha)  # кол-во эл в алфавите
p = lab1_a.generateP(m)

print("m: " + str(m))

for i in range(0, m):
    print(f'{alpha[i]} {p[i]}')

print("Равномерное распределение: ")
lab1_a.even(m, n)
print(f'H({m}) = {lab1_a.entropyEven(m)}')
lab1_a.REven(m)

print("Неравномерное распределение: ")
lab1_a.uneven(m, n, p)
print(f'H({m}) = {lab1_a.entropyUneven(m, p)}')
lab1_a.RUneven(m, p)

print("Алфавит источника из одного символа: ")
# madeAlpha = makeAlpha(message)#взять все буквы из сообщения
madeAlpha = alpha[int(random.uniform(0, n))]
print('Алфавит : ' + str(madeAlpha[0]))
lab1_a.even(len(madeAlpha), n)
print(f'H({len(madeAlpha)}) = {lab1_a.entropyEven(len(madeAlpha))}')
if len(madeAlpha) > 1:
    lab1_a.REven(m)
