class Node:
    def __init__(self):
        self.a = None
        self.b = None
        self.p = None
        self.ch = None
        self.code = None


def generate_tree(inp):
    nodes = []
    for i in inp:
        node = Node()
        node.ch = i[0]
        node.p = i[1]
        nodes.append(node)

    while len(nodes) > 1:
        nodes.sort(key=lambda x: x.p, reverse=True)

        a = nodes[-1]
        b = nodes[-2]
        a.code = "0"
        b.code = "1"
        del nodes[-1]
        del nodes[-1]

        node = Node()
        node.a = a
        node.b = b
        node.p = a.p + b.p

        nodes.append(node)

    result = nodes[0]
    result.code = ""

    return result


def _extract_tree(node, code, result):
    if node.ch is not None:
        result.append((node.ch, code + node.code))
        return
    if node.a is not None:
        _extract_tree(node.a, code + node.code, result)
    if node.b is not None:
        _extract_tree(node.b, code + node.code, result)
    return result


def extract_tree(node):
    return sorted(_extract_tree(node, "", []), key=lambda x: x[0])


def huffman_code(inp):
    return sorted(_extract_tree(generate_tree(inp), "", []), key=lambda x: x[0])


def print_tree(node, depth=0):
    if node.a is not None:
        print_tree(node.a, depth + 1)
    if node.ch is not None:
        print(('\t\t\t' * depth) + "[" + '{0:g}'.format(float(node.p)) + "] " + node.ch + " (" + node.code + ")")
    else:
        print(('\t\t\t' * depth) + "[" + '{0:g}'.format(float(node.p)) + "]" + " (" + node.code + ")")

    if node.b is not None:
        print_tree(node.b, depth + 1)
