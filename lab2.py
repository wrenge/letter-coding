import lab1_a
import lab2_a
import lab2_b
import lab2_c
import lab2_d


def avg_length(inp, code):
    result = 0
    for i in range(0, len(inp)):
        result += inp[i][1] * len(code[i][1])
    return result


def rel_eff(entropy, avg_len):
    return entropy / avg_len


def red_factor(entropy, avg_len):
    return 1 - rel_eff(entropy, avg_len)


def red_source(entropy, entropy_max):
    return 1 - (entropy / entropy_max)


def analyze(inp, code, entropy):
    print("Средняя длина слова")
    l_avg = avg_length(inp, code)
    print(f"l_avg = {l_avg}")
    print("Коэффициент относительной эффективности")
    print(f"K = {rel_eff(entropy, l_avg)}")
    print("Коэффициент избыточности кода")
    print(f"R = {red_factor(entropy, l_avg)}")


def encrypt_example(msg, code):
    print(f"Зашифрованное {msg}:\t\t\t\t" + lab2_d.m2c(msg, code))
    print(f"Зашифрованное {msg} с пробелами:\t" + lab2_d.m2c_nice(msg, code))
    print(f"Расшифрованное {msg}:\t\t\t" + lab2_d.c2m(lab2_d.m2c(msg, code), code))


def print_code(code):
    for i in code:
        print(str(i))


def run(inp, msg):
    sorted_inp = sorted(inp, key=lambda x: x[1], reverse=True)
    print("Входные значения:")
    for i in sorted_inp:
        print(str(i))
    print()
    print("Равномерный метод")
    print("Длина: log2(Количество символов) + 1")
    even_code = lab2_a.even_code(sorted_inp)
    print("Метод Шенона-Фано")
    fano_code = lab2_b.fano_code(sorted_inp)
    print("Метод Хаффмана")
    huffman_tree = lab2_c.generate_tree(sorted_inp)
    huffman_code = lab2_c.extract_tree(huffman_tree)
    lab2_c.print_tree(huffman_tree)
    p = []
    for i in sorted_inp:
        p.append(i[1])
    m = len(sorted_inp)
    print("Энтропия источника")
    entropy = lab1_a.entropyUneven(m, p)
    print(f"H(A) = {entropy}")
    print("Избыточность источника")
    print(f"R = {red_source(entropy, lab1_a.entropyEven(m))}")
    print("\nРавномерный метод")
    print_code(even_code)
    analyze(sorted_inp, even_code, entropy)
    encrypt_example(msg, even_code)
    print("\nМетод Шенона-Фано")
    print_code(fano_code)
    analyze(sorted_inp, fano_code, entropy)
    encrypt_example(msg, fano_code)
    print("\nМетод Хаффмана")
    print_code(huffman_code)
    analyze(sorted_inp, huffman_code, entropy)
    encrypt_example(msg, huffman_code)


# inp = [('a', 0.3),
#        ('e', 0.1),
#        ('d', 0.13),
#        ('b', 0.2),
#        ('c', 0.2),
#        ('f', 0.07)]
# msg = "babaedcf"
# run(inp, msg)
#
# inp = [('a', 0.4),
#        ('c', 0.2),
#        ('b', 0.4)]
# msg = "babacacabbb"
# run(inp, msg)

inp = [('З', 0.1),
       ('а', 0.1),
       ('ч', 0.1),
       ('ё', 0.1),
       ('т', 0.1),
       (' ', 0.1),
       ('п', 0.1),
       ('л', 0.1),
       ('и', 0.1),
       ('з', 0.1)]
msg = "Зачёт плиз"
run(inp, msg)
