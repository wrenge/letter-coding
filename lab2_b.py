# Разделять по сумме. Сумма > 0.5 от полной суммы массива в отдельную группу
def fano(inp, code, result):
    if len(inp) < 2:
        # print((len(code) - 1) * '\t\t\t' + str(inp[0]))
        result.append((inp[0][0], code))
        return
    bank = sum(item[1] for item in inp)
    i = 0
    while sum(item[1] for item in inp[len(inp) - i:]) < bank / 2 and i < len(inp) - 1:
        i += 1

    a = inp[:len(inp) - i]
    b = inp[len(inp) - i:]

    if len(code) > 0:
        print(code + ":")
    print('\t\t\t' + str(a) + " - 0")
    print('\t\t\t' + str(b) + " - 1")
    fano(a, code + '0', result)
    fano(b, code + '1', result)
    return result


def fano_code(inp):
    return fano(inp, "", [])
